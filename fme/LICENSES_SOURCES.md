# Licenses
The following chapter lists the licenses for data usage of geonames.org as well as for the country specific data sources.

## Geonames
The following Licensing information is published on the geonames.org website and has to be followed:
This work is licensed under a Creative Commons Attribution 3.0 License,
see http://creativecommons.org/licenses/by/3.0/
The Data is provided "as is" without warranty or any representation of accuracy, timeliness or completeness.
## DE
- Website URL: http://www.geodatenzentrum.de/geodaten/gdz_rahmen.gdz_div?gdz_spr=deu&gdz_akt_zeile=5&gdz_anz_zeile=1&gdz_unt_zeile=20&gdz_user_id=0
- Full License Documentation: http://www.geodatenzentrum.de/docpdf/geonutzv.pdf Attribution needed in this form:  GeoBasis-DE / BKG <Jahr des letzten Datenbezugs> (Daten verndert)

## HR 
- Website: http://www.dgu.hr/
- License not found on website or WFS Response
- Data: WFS Endpoint: http://cgn.dgu.hr/deegree-wfs/services/wfs?service=WFS&request=GetCapabilities&version=2.0.0 

## LU
- Website: http://inspire.geoportail.lu/geoportal/
- Multiple License found
oFor the entire portal http://wiki.geoportal.lu/doku.php?id=en:mcg_1
oAccess constraints at WFS Response: Fees:no conditions apply AccessConstraints:None
oCatalog Level: http://inspire.geoportail.lu/geoportal/catalog/search/resource/details.page?uuid={56EB2DFA-14B7-4943-819A-E4A16FB3B389}: Constraints: For publications of all kind a written authorization has to be asked from "Administration du Cadastre et de la Topographie, Luxembourg" Legal Constraints: Access Constraints: Other Restrictions Other Constraints: (e) intellectual property rights;
- Dataset WFS Endpoint: http://wsinspire.geoportail.lu/gn_ds?service=WFS&request=getCapabilities

## PT
- Website: http://mapas.igeo.pt/ 
- License not found

## PL
- Website: http://www.codgik.gov.pl/index.php/darmowe-dane/prng.html
- License not found, but website says free of charge

## ES
- Website: http://www.idee.es/web/guest/servicios 
- Data: http://www.ign.es/wfs-inspire/unidades-administrativas?service=WFS&request=GetCapabilities 
- Taken from WFS getCapabilities Response:  Se permite el acceso a este servicio en cualquier caso siempre que se mencionen la autora y propiedad del IGN del siguiente modo:  Instituto Geogrfico Nacional de Espaa

## DK
- Full License Documentation should be here: http://download.kortforsyningen.dk/content/vilk%C3%A5r-og-betingelser 
- Example: Authority enforces its copyright. When used, the user: at a reasonable place suitable for the distribution media, insert the following:  1. "Contains data from Danish Geodata Agency" second The name of the data set (one) third Time when the data set (s) are taken by the Authority, or if there is a data service. (Eg: "Contains data from Danish Geodata Agency, Kort10, January 2013". Or: "Contains data from Danish Geodata Agency, cadastral map, WMS service" )

## CH
- http://data.geo.admin.ch/ch.swisstopo.swissnames3d/ 
- License: http://www.swisstopo.admin.ch/internet/swisstopo/en/home/swisstopo/legal_bases/copyright.html 
